# fonts-willy
Collection of fonts that suits Willy's need : )

Fonts can be placed in the following directory:
```
/usr/share/fonts/
~/.fonts/
```

Reference: https://github.com/ryanoasis/nerd-fonts/
